# Electron Tone Synth

Sequencer-Synthesizer built with <a href="https://tonejs.github.io" target="_blank">Tone.js</a>, powered by <a href="https://www.electronjs.org/" target="_blank">Electron</a>, built with <a href="https://svelte.dev" target="_blank">Svelte</a> and <a href="https://tailwindcss.com/" target="_blank">TailwindCSS</a>.

## Commands

- `npm run dev`: Runs Svelte in dev mode
- `npm run start`: Runs Svelte in production mode
- `npm run electron`: Runs Svelte with electron in dev mode
- `npm run build`: Runs Svelte compiler
- `npm run dev:package`: Creates an Electron package (you can inspect the contents)
- `npm run package`: Creates a distributable Electron package
