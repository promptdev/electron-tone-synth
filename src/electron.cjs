require('dotenv').config();

const { app, ipcMain, BrowserWindow } = require('electron');
const serve = require('electron-serve');
const ws = require('electron-window-state');
const fs = require('fs');
const path = require('path');

try {
  require('electron-reloader')(module);
} catch {
  console.log('require electron-reloader failed');
}

const loadURL = serve({ directory: '.' });
const port = process.env.PORT;
const isdev = !app.isPackaged || process.env.NODE_ENV === 'development';

let mainWindow;

function loadSvelte(port) {
  mainWindow.loadURL(`http://127.0.0.1:${port}`).catch((err) => {
    setTimeout(() => {
      loadSvelte(port);
    }, 200);
  });
}

function createMainWindow() {
  let mws = ws({
    defaultWidth: 1024,
    defaultHeight: 768,
  });

  mainWindow = new BrowserWindow({
    x: mws.x,
    y: mws.y,
    width: mws.width,
    height: mws.height,

    webPreferences: {
      nodeIntegration: false,
      contextIsolation: true,
      enableRemoteModule: false,
      preload: path.join(__dirname, 'preload.cjs'),
      devTools: isdev,
    },
  });

  mainWindow.once('close', () => {
    mainWindow = null;
  });

  if (!isdev) {
    mainWindow.removeMenu();
  } else {
    mainWindow.webContents.openDevTools();
  }

  mws.manage(mainWindow);

  if (isdev) {
    loadSvelte(port);
  } else {
    loadURL(mainWindow);
  }
}

app.once('ready', createMainWindow);

app.on('activate', () => {
  if (!mainWindow) createMainWindow();
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit();
});

// bridging with app
ipcMain.on('toMain', (event, args) => {
  console.log('toMain args:', args);

  mainWindow.webContents.send('fromMain', 'whateverObj');
  // fs.readFile('path/to/file', (error, data) => {
  //   // Do something with file contents
  //
  //   // Send result back to renderer process
  //   mainWindow.webContents.send('fromMain', 'whateverObj');
  // });
});
