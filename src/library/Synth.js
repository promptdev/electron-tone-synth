import * as Tone from 'tone';
import AudioKeys from 'audiokeys';
import activeNote from './stores/activeNote';
import Config from './Config';

const keyboard = new AudioKeys();
const synthType = localStorage.getItem('synthType') || Config.synthType;

export default class Synth {
  constructor() {
    this.instrument = new Tone[synthType]();
    this.options = {};
    this.setKeyboard();
    this.init();
  }

  updateInstrument(synthType, options) {
    if (this.instrument) {
      this.instrument.dispose();
    }

    this.instrument = new Tone[synthType](options);
    this.init();
  }

  setGain(gain = localStorage.getItem('gain') || Config.gain) {
    return new Tone.Gain(gain);
  }

  setKeyboard() {
    keyboard.down((note) => {
      this.instrument.triggerAttack(note.frequency);
      // Updates active keyboard note store
      activeNote.update(() => note);
    });

    keyboard.up(() => {
      this.instrument.triggerRelease();
      // Resets keyboard note store
      activeNote.set(null);
    });
  }

  init() {
    this.gain = this.setGain();
    this.instrument.connect(this.gain);
    this.instrument.toDestination();
  }
}
