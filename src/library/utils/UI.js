import ClassGroup from 'classgroup';

const button = {
  p: 'cursor-pointer border-b-2 shadow',
  a: 'active:border-b active:translate-y-0.5 active:shadow-inner',
  o: 'outline-none focus:outline-none',
  t: 'transition-all',
};

const noOutline = {
  o: 'outline-none focus:outline-none',
};

const UI = ClassGroup({
  button,
  noOutline,
});

export default UI;
